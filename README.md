## Obfuscation library

### License

This software is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT)

### Requirements
* PHP 7.2+

Works in conjunction with the `smorken-obfuscate` javascript library.
```
$ npm install smorken-obfuscate
```

##### Examples
```php
<?php
$emails = ['foo@example.org', 'bar@example.org', 'fizbuz@example.net'];
$ob = new \Smorken\Obfuscate\Obfuscate();
foreach ($emails as $email): ?>
    <a class="clr" data-smob="<?php echo $ob->obfuscate($email); ?>" data-smob-overwrite data-smob-prepend="mailto:"></a>
<?php endforeach; ?>
<script src="assets/js/smob.var.js"></script>
<script>
document.addEventListener('DOMContentLoaded', function () {
    var smob = new SmOb.Obfuscate();
    var smobdom = new SmOb.Dom(smob);
    smobdom.handle(document.querySelectorAll('.clr'), 'clear');
});
</script>
```
