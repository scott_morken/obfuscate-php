<?php

namespace Tests\Smorken\Obfuscate\Unit;

use PHPUnit\Framework\TestCase;
use Smorken\Obfuscate\Obfuscate;

class ObfuscateTest extends TestCase
{
    public function testClearStringWithDefaults()
    {
        $sut = $this->getSut();
        $this->assertEquals('email@example.org',
            $sut->clear('124/132/120/128/131/87/124/143/120/132/135/131/124/69/134/137/126'));
    }

    public function testClearStringWithRotation()
    {
        $sut = $this->getSut();
        $this->assertEquals('email@example.org',
            $sut->clear('128/136/124/132/135/91/128/147/124/136/139/135/128/73/138/141/130', 10));
    }

    public function testClearStringWithSplitAt()
    {
        $sut = $this->getSut();
        $this->assertEquals('email@example.org',
            $sut->clear('124::132::120::128::131::87::124::143::120::132::135::131::124::69::134::137::126', null,
                '::'));
    }

    public function testObfuscateAndClear()
    {
        $sut = $this->getSut(['rotation' => 30, 'split_at' => '|']);
        $test = 'This is a test sentence!';
        $o = $sut->obfuscate($test);
        $this->assertEquals('138|158|159|169|86|159|169|86|151|86|170|155|169|170|86|169|155|164|170|155|164|153|155|87',
            $o);
        $this->assertEquals($test, $sut->clear($o));
    }

    public function testObfuscateAndClearWithDifferentSettings()
    {
        $sut = $this->getSut();
        $test = 'This is a test sentence!';
        $o = $sut->obfuscate($test);
        $this->assertEquals('114/134/135/145/62/135/145/62/127/62/146/131/145/146/62/145/131/140/146/131/140/129/131/63',
            $o);
        $this->assertEquals('Uijt!jt!b!uftu!tfoufodf"', $sut->clear($o, 5));
    }

    public function testObfuscateStringWithDefaults()
    {
        $sut = $this->getSut();
        $this->assertEquals('124/132/120/128/131/87/124/143/120/132/135/131/124/69/134/137/126',
            $sut->obfuscate('email@example.org'));
    }

    public function testObfuscateStringWithRotation()
    {
        $sut = $this->getSut();
        $this->assertEquals('128/136/124/132/135/91/128/147/124/136/139/135/128/73/138/141/130',
            $sut->obfuscate('email@example.org', 10));
    }

    public function testObfuscateStringWithSplitAt()
    {
        $sut = $this->getSut();
        $this->assertEquals('124::132::120::128::131::87::124::143::120::132::135::131::124::69::134::137::126',
            $sut->obfuscate('email@example.org', null, '::'));
    }

    /**
     * @return \Smorken\Obfuscate\Obfuscate
     */
    protected function getSut(array $config = [])
    {
        return new Obfuscate($config);
    }
}
