<?php

namespace Smorken\Obfuscate\Contracts;

interface Obfuscate
{
    public function clear(string $str, ?int $rotation = null, ?string $split_at = null): string;

    public function obfuscate(string $str, ?int $rotation = null, ?string $split_at = null): string;
}
