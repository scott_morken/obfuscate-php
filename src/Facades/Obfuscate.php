<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 4/4/14
 * Time: 6:25 PM
 */

namespace Smorken\Obfuscate\Facades;

class Obfuscate extends \Illuminate\Support\Facades\Facade
{
    protected static function getFacadeAccessor()
    {
        return \Smorken\Obfuscate\Contracts\Obfuscate::class;
    }
}
