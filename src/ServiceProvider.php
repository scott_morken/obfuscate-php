<?php

namespace Smorken\Obfuscate;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    /**
     * Bootstrap the application events.
     */
    public function boot(): void
    {
        $this->bootConfig();
    }

    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->app->bind(\Smorken\Obfuscate\Contracts\Obfuscate::class, function ($app) {
            $config = $app['config']->get('obfuscate', []);

            return new Obfuscate($config);
        });
    }

    protected function bootConfig(): void
    {
        $config = __DIR__.'/../config/config.php';
        $this->mergeConfigFrom($config, 'obfuscate');
        // @phpstan-ignore function.notFound
        $this->publishes([$config => config_path('obfuscate.php')], 'config');
    }
}
