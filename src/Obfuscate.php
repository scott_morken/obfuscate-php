<?php

namespace Smorken\Obfuscate;

class Obfuscate implements \Smorken\Obfuscate\Contracts\Obfuscate
{
    /**
     * @var array<string, int|string>
     */
    protected array $config = [
        'rotation' => 6,
        'split_at' => '/',
        'encoding' => 'UTF-8',
    ];

    /**
     * @param  array<string, int|string>  $config
     */
    public function __construct(array $config = [])
    {
        foreach ($config as $k => $v) {
            $this->setConfig($k, $v);
        }
    }

    public function clear(string $str, ?int $rotation = null, ?string $split_at = null): string
    {
        [$rotation, $split_at] = $this->getRotationAndSplitAt($rotation, $split_at);
        /** @var non-empty-string $split_at */
        $split = explode($split_at, $str);
        $clear = fn ($v) => mb_chr((int) $v - (int) $rotation - count($split),
            (string) $this->getConfig('encoding', 'UTF-8'));

        return implode('', array_map($clear, $split));
    }

    public function obfuscate(string $str, ?int $rotation = null, ?string $split_at = null): string
    {
        [$rotation, $split_at] = $this->getRotationAndSplitAt($rotation, $split_at);
        $split = str_split($str);
        $obfuscate = fn ($v) => mb_ord($v,
            (string) $this->getConfig('encoding', 'UTF-8')) + (int) $rotation + count($split);

        return implode($split_at, array_map($obfuscate, $split));
    }

    protected function getConfig(string $key, string|int|null $default = null): string|int|null
    {
        if (array_key_exists($key, $this->config)) {
            return $this->config[$key];
        }

        return $default;
    }

    protected function setConfig(string $key, string|int $value): void
    {
        if (empty($value)) {
            return;
        }
        $this->config[$key] = $value;
    }

    /**
     * @phpstan-return array{0: int, 1: string}
     */
    protected function getRotationAndSplitAt(?int $rotation, ?string $split_at): array
    {
        $rotation ??= (int) $this->getConfig('rotation');
        $split_at ??= (string) $this->getConfig('split_at');

        return [$rotation, $split_at];
    }
}
